#include "Server.hpp"
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "BitStream.h"
#include "Utilities.h"
#include "gl_core_4_4.h"
#include <GLFW/glfw3.h>
#include "Game.hpp"

#include <vector>

const static char c_csUserTablePath[] = "userinfo.txt";
const static char c_csPassword[] = "password";
const static unsigned short c_huPort = 57371;

// Load a user table from a given file name.
// If no file name is given, then user the default one.
void Server::LoadUserTable(const char *m_csPath)
{
	if (!m_csPath) {
		printf("Error in Server::LoadUserTable(): File path not set!\nUsing default path instead...\n");
		m_csPath = c_csUserTablePath;
	}

	FILE *usertable = fopen(m_csPath, "r");
	int err = 0;

	if (!usertable) {
		printf("Error in Server::LoadUserTable(): File not found at \"%s\"!\n", m_csPath);
		printf("Creating new file at \"%s\"...", m_csPath);
		SaveUserTable();
		return;
	}

	unsigned int *numusers = &m_uUITCount;
	err = fscanf(usertable, "%*s %*s %*s %u", numusers);

	if (err < 0) {
		printf("Error in Server::LoadUserTable(): EOF reached unexpectedly!\n");
	}

	m_xUserInfoTable = (RegisteredUser*)malloc(sizeof(RegisteredUser) * m_uUITCount);
	memset(m_xUserInfoTable, 0, sizeof(RegisteredUser) * m_uUITCount);

	printf("Number of users in user table: %u\n", m_uUITCount);

	for (unsigned int i = 0; i < m_uUITCount; ++i) {

		char *name = m_xUserInfoTable[i].m_csName;
		unsigned int *timesconnected = &m_xUserInfoTable[i].m_uTimesConnected;

		if (fscanf(usertable, "%*s %s %*s %u", name, timesconnected) < 0) break;
		printf("Name: %s\nTimes connected: %u\n", name, *timesconnected);
	}

	fclose(usertable);
}

void Server::SaveUserTable()
{
	if (!m_csUserTableFilePath) {
		printf("Error in Server::SaveUserTable(): File path not set!\nUsing default path instead...\n");
		m_csUserTableFilePath = c_csUserTablePath;
	}

	FILE *usertable = fopen(m_csUserTableFilePath, "w");

	printf("Number of users in user table: %u\n", m_uUITCount);
	fprintf(usertable, "Number of users: %u\n", m_uUITCount);

	for (unsigned int i = 0; i < m_uUITCount; ++i) {
		fprintf(usertable, "\nUsername: %s\nAmount: %u\n", m_xUserInfoTable[i].m_csName, m_xUserInfoTable[i].m_uTimesConnected);
		printf("\nUsername: %s\nAmount: %u\n", m_xUserInfoTable[i].m_csName, m_xUserInfoTable[i].m_uTimesConnected);
	}

	fclose(usertable);
}

void Server::SerializeEntities(RakNet::BitStream *a_xBitstream)
{
	if (!a_xBitstream) return;

	unsigned int size = level_size(m_xLevel);
	a_xBitstream->Write(size);

	for (unsigned int i = 0; i < size; ++i) {
		Entity *ent = level_at(m_xLevel, i);
		if (ent) {
			ent->Serialize(true, a_xBitstream);
		}
	}
}

void Server::Network()
{
	RakNet::Packet *packet = m_xPeer->Receive();
	RakNet::MessageID msgid = 0;

	while (packet)  {
		RakNet::BitStream bsin(packet->data, packet->length, false);
		
		unsigned int size = level_size(m_xLevel);
		for (unsigned int i = 0; i < size; ++i) {

			Entity *ent = level_at(m_xLevel, i);
			if (ent) {
				ent->Packet(packet);
			}
		}

		bsin.Read(msgid);
		switch (msgid) {
			case ID_CLIENT_CREDENTIALS: {
				puts("Client credentials recieved. Processing...");

				RakNet::BitStream bsout;
				bool fresh = true;
				unsigned int timesconnected = 0;

				RakNet::RakString name;
				bsin.Read(name);

				if (!name) {
					printf("Error in Server::Network while processing client credentials: name was null!\n");
					break;
				}

				ConnectedUser *connusr   = nullptr;
				bool           connected = false;

				// Attempt to find out if this client's username is already in use by an existing
				// player on the server:
				for (unsigned int i = 0; i < m_uConnUserCount; ++i) {
					if (!strcmp(name.C_String(), m_aConnectedUser[i].m_csName)) {
						connected = true;
					}
				}

				// Kick our player from the server if his name's in use:
				if (connected) {
					printf("User \"%s\" already connected! Disconnecting...\n", name.C_String());
					m_xPeer->CloseConnection(packet->systemAddress, true);
					break;
				}
				// Otherwise add his credentials to the connected uesr list:
				else {
					
					m_aConnectedUser[m_uConnUserCount].m_xGuid = packet->guid;
					strcpy(m_aConnectedUser[m_uConnUserCount].m_csName, name.C_String());

					++m_uConnUserCount;
				}

				// Try to find this client's credentials, to see if he is a new guest or not:
				for (unsigned int i = 0; i < m_uUITCount; ++i) {
					if (!strcmp(m_xUserInfoTable[i].m_csName, name.C_String())) {
						fresh = false;
						timesconnected = ++m_xUserInfoTable[i].m_uTimesConnected;

						printf("Old user \"%s\" connected.\n", name.C_String());
						printf("Times connected: %u\n", timesconnected);
					}
				}

				// This is the first time this client has tried to connect to us;
				// add it to the list:
				if (fresh) {

					++m_uUITCount;
					m_xUserInfoTable = (RegisteredUser*)realloc(m_xUserInfoTable, sizeof(RegisteredUser) * m_uUITCount);

					++timesconnected;
					strcpy(m_xUserInfoTable[m_uUITCount - 1].m_csName, name.C_String());
					m_xUserInfoTable[m_uUITCount - 1].m_uTimesConnected = timesconnected;

					printf("New user \"%s\" connected.\n", name.C_String());
				}

				bsout.Write((unsigned char)ID_SERVER_CONNECTION_PASS);
				bsout.Write(timesconnected);

				m_xPeer->Send(&bsout, IMMEDIATE_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			} break;
			case ID_CONNECTION_LOST: {
				bool found = false;

				for (unsigned int i = 0; i < m_uConnUserCount; ++i) {
					ConnectedUser *usr = m_aConnectedUser + i;
					if (usr->m_xGuid == packet->guid) {

						found = true;
						printf("User \"%s\" lost connection!\n", usr->m_csName);

						memmove(usr, usr + 1, sizeof(ConnectedUser) * c_uMaxClients - i);
						--m_uConnUserCount;

						break;
					}
				}
				if (!found) printf("Unknown user lost connection!\n");
			} break;
			case ID_DISCONNECTION_NOTIFICATION: {
				bool found = false;

				for (unsigned int i = 0; i < m_uConnUserCount; ++i) {
					ConnectedUser *usr = m_aConnectedUser + i;
					if (usr->m_xGuid == packet->guid) {

						found = true;
						printf("User \"%s\" disconnected!\n", usr->m_csName);

						if (i < m_uConnUserCount - 1) {
							memmove(usr, usr + 1, sizeof(ConnectedUser) * c_uMaxClients - (i + 1));
						}
						
						--m_uConnUserCount;

						break;
					}
				}
				if (!found) printf("Unknown user disconnected!\n");
			} break;
		}

		m_xPeer->DeallocatePacket(packet);
		packet = m_xPeer->Receive();
	}

	static float ptime = 0.0f;
	ptime += m_fDeltaTime;

	if (ptime > 1.0f / c_fPcktsPrSec) {
		ptime = 0;

		unsigned short numconn = m_xPeer->NumberOfConnections();
		if (numconn > 0) {

			RakNet::BitStream bsout;
			bsout.Write((unsigned char)ID_SERVER_ENTITIES);

			SerializeEntities(&bsout);

			RakNet::SystemAddress *addr = new RakNet::SystemAddress[numconn];
			m_xPeer->GetConnectionList(addr, &numconn);

			for (unsigned int i = 0; i < numconn; ++i) {
				m_xPeer->Send(&bsout, HIGH_PRIORITY, RELIABLE_ORDERED, 0, addr[i], false);
			}

			delete addr;
			addr = nullptr;
		}
	}
}

bool Server::Start(int argc, char *argv[])
{
	unsigned short port = c_huPort;
	const char *password = c_csPassword;
	const char *path = c_csUserTablePath;

	if (argc < 4) {
		printf("Warning: No table path provided. Default user table path is %s\n", c_csUserTablePath);
	}
	else {
		path = argv[3];
		printf("Path: %s\n", path);
	}

	if (argc < 3) {
		printf("Warning: No password provided. Default password is %s\n", c_csPassword);
	}
	else {
		password = argv[2];
		printf("Password: %s\n", password);
	}

	if (argc < 2) {
		printf("Warning: No port provided. Default port is %hu\n", c_huPort);
	}
	else {
		port = atoi(argv[1]);
		printf("Port: %hu\n", port);
	}

	LoadUserTable(path);
	m_xLevel = level_create(c_uMaxClients + 1);

	int err = level_add(m_xLevel, &Entity("Game", GetNewID()));
	if (err > -1) {

		Entity *ent = level_at(m_xLevel, err);
		if (ent) {
			Game *game = new Game(ent, m_xLevel);
			if (game) {
				ent->AddComponent(game);
			}
		}
	}

	// Initialise RakNet:
	m_xPeer = RakNet::RakPeerInterface::GetInstance();
	if (!m_xPeer) return false;
	
	RakNet::SocketDescriptor socket = RakNet::SocketDescriptor(port, NULL);
	m_xPeer->SetMaximumIncomingConnections(c_uMaxClients);
	m_xPeer->SetIncomingPassword(password, strlen(password));
	m_xPeer->Startup(c_uMaxClients, &socket, 1);

	RakNet::RakNetGUID guid = m_xPeer->GetMyGUID();
	printf("GUID: %llu\n", guid.g);

	return true;
}

void Server::Update()
{
	unsigned int size = level_size(m_xLevel);
	for (unsigned int i = 0; i < size; ++i) {
		Entity *ent = level_at(m_xLevel, i);
		if (ent) {
			ent->Update(m_fDeltaTime);
		}
	}
}

void Server::Run()
{
	glfwInit();
	Utility::resetTimer();
	
	do {
		m_fDeltaTime = Utility::tickTimer();

		Network();
		Update();
	} 
	while (true);
}

Server::~Server()
{
	level_destroy(m_xLevel);

	SaveUserTable();
	free(m_xUserInfoTable);
	RakNet::RakPeerInterface::DestroyInstance(m_xPeer);
}
