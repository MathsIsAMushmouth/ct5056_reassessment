#ifndef SERVER_INCLUDED
#define SERVER_INCLUDED

#include "Application.h"

#include "Server.hpp"
#include "Entity.hpp"
#include "Player.hpp"
#include "Common.h"

#include "RakNetTypes.h"
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "BitStream.h"
#include "Level.h"

struct RegisteredUser
{
	char m_csName[c_uNameLength];
	unsigned int m_uTimesConnected;
};

struct ConnectedUser
{
	char m_csName[c_uNameLength];
	RakNet::RakNetGUID m_xGuid;
};

/* Attributes shared between Server and client: 
   - Entities
   - <entity tally>
   - FindEntity()
   - RemoveEntity()
   - AddEntity()
   */


/*	- Server -
	 The server class is responsible for managing the metadata can connection status
	of our client's, as well as managing the master state of the game engine, and
	relaying any packet's sent by our client's to our entities.
*/
class Server
{
public:
	Server() {}
	~Server();

	bool
	Start
		(int argc, char *argv[]);

	void
	Run
		();

private:

	LEVEL *m_xLevel = nullptr;

	ConnectedUser m_aConnectedUser[c_uMaxClients] = { 0 };
	unsigned int m_uConnUserCount = 0;

	RegisteredUser *m_xUserInfoTable = nullptr;
	unsigned int m_uUITCount = 0;
	const char *m_csUserTableFilePath = nullptr;

	RakNet::RakPeerInterface *m_xPeer = nullptr;

	float m_fDeltaTime;

	void
		Network
		(void);

	void
		Update
		(void);

	void
		LoadUserTable
		(const char *m_csPath);

	void
		SaveUserTable
		(void);

	void
		SerializeEntities
		(RakNet::BitStream *a_xBitstream);
};


#endif