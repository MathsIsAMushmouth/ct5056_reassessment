#include "Server.hpp"

int main(int argc, char *argv[])
{
	Server *server = nullptr;

	server = new Server();

	if (server->Start(argc, argv)) {
		server->Run();
	}
	delete(server);

	return 0;
}