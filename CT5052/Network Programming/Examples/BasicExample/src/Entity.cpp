#include "Entity.hpp"

Entity::Entity(const char *a_sName) : m_sName(a_sName)
{}

void Entity::Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream)
{
	// Write the name to the stream:
	if(a_bWrite) {
		a_xBitstream->Write(m_sName);
		a_xBitstream->Write((unsigned char)m_vComponents.size());
	}
	else {
		RakNet::RakString newname;

		a_xBitstream->Read(m_sName);

		unsigned char num_ents = 0;
		a_xBitstream->Read((unsigned char)num_ents);
	}

	// SerializeEntities each and every entity:
	for (unsigned int i = 0; i < m_vComponents.size(); ++i)
	{
		a_xBitstream->Write(m_vComponent[i]->m_eID);

		m_vComponent[i]->Serialize(a_bWrite, a_xBitstream);
	}
}

Entity::~Entity()
{
	m_vComponents.clear();

	delete(&m_sName);
}

void Entity::Update(const float a_fDeltaTime)
{
	for (unsigned int i = 0; i < m_vComponents.size(); ++i)
	{
		Component *indexedComponent = m_vComponent[i];

		indexedComponent->Update(a_fDeltaTime);
	}
}

void Entity::Draw()
{
	for (unsigned int i = 0; i < m_vComponents.size(); ++i)
	{
		Component *indexedComponent = m_vComponent[i];

		indexedComponent->Draw();
	}
}

const char * const Entity::Name() const
{
	return m_sName;
}