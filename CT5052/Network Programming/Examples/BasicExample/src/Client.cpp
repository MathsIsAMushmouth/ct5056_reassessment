#include "Client.hpp"
#include "Gizmos.h"
#include "Utilities.h"
#include "gl_core_4_4.h"
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>
#include "BitStream.h"
#include <stdio.h>
#include <chrono>
//#include <thread>
#include "RakNetTypes.h"

#include "Renderer.hpp"
#include "Player.hpp"
#include "Input.hpp"
#include "Game.hpp"

// These strings are printed after peer->Connect(). Each
// entry corresponds to a valid return value:
const unsigned int _connAttemptResultCodes_num = 6;
const char *_connectionAttemptResultCodes[] =
{
	"Connection attempt started...\n",
	"Error: Invalid Parameter\n",
	"Error: Cannot resolve host name\n",
	"Error: Already connected to endpoint\n",
	"Error: Connection attempt already in progress\n",
	"Error: Security initialisation failed\n"
};

#define DEFAULT_SCREENWIDTH 800
#define DEFAULT_SCREENHEIGHT 600
#define MAX_CLIENTS 5

// Packet Format:
// ID
// ENTITY
// {
//   MORE
//   c_uNameLength
//   c_uNameLength
//   NAME[] ..
//   COMPNUM
//   COMPONENTS[]
//   {
//     COMPNUM
//     TYPE
//     LENGTH
//     LENGTH
//     DATA[] ..
//   }
//   ..
// }
// [MORE]/!

Client::Client()
{
	m_xPeer = RakNet::RakPeerInterface::GetInstance();
}

Client::~Client() { }

// This function will deserialise a list of entities from a given packet.
// It's expected to take a long time, but will only be used for the startup
// portion of the game:
void Client::DeSerializeEntities(RakNet::BitStream *a_xBitstream)
{
	ComponentID  type;
	unsigned int num_components = 0;
	unsigned int num_entities = 0;

	a_xBitstream->Read(num_entities);

	level_destroy(m_xLevel);
	m_xLevel = level_create(num_entities);

	// Continue this loop until the packet has been exhausted:
	for (int e = 0; e < num_entities; ++e) {

		RakNet::RakString name;
		unsigned int uid = 0;

		a_xBitstream->Read(uid);
		a_xBitstream->Read(name);
		a_xBitstream->Read(num_components);

		// Create our new entity:
		Entity entity(name.C_String(), uid);
		int err = level_add(m_xLevel, &entity);

		Entity *ent = level_at(m_xLevel, err);

		// Deserialize the components:
		

		for (int c = 0; c < num_components; ++c) {
			a_xBitstream->Read(type);

			// A pointer to the component we'll be creating:
			Component *component = nullptr;

			switch (type)
			{
			case RENDERER:
				component = new Renderer(ent, glm::vec3(5, 5, 5), glm::vec3(7, 7, 7));
				break;
			case PLAYER:
				component = new Player(ent, m_xPeer->GetMyGUID());
				break;
			case GAME:
				component = new Game(ent, m_xLevel);
				break;
			case PICKUP:
				component = new Pickup(ent, glm::vec3(0, 0, 0));
				break;
			default:
				printf("A component ID of %hhu was found!\n", type);
				break;
			}

			if (component) {
				component->Serialize(false, a_xBitstream);
				ent->AddComponent(component);
				component = nullptr;
			}
		}
	}
}

// This function is used to decode a packet.
void Client::Network()
{
	RakNet::Packet *packet = m_xPeer->Receive();
	RakNet::MessageID msgid = 0;

	while (packet) {
		RakNet::BitStream bsin(packet->data, packet->length, false);
		bsin.Read(msgid);

		unsigned int size = level_size(m_xLevel);
		for (unsigned int i = 0; i < size; ++i) {

			Entity *ent = level_at(m_xLevel, i);
			if (ent) {
				ent->Packet(packet);
			}
		}

		switch (msgid)
		{
		case ID_CLIENT_INPUT: {
			// Update the correct place in our input buffer with the new input:
			RakNet::RakString name;
			unsigned int input = 0;

			bsin.Read(name);
			bsin.Read(input);

			unsigned int size = level_size(m_xLevel);
			for (unsigned int i = 0; i < size; ++i) {
				Entity *ent = level_at(m_xLevel, i);
				if (ent) {
					ent->Input(input);
				}
			}
		} break;
		case ID_SERVER_ENTITIES: {
			DeSerializeEntities(&bsin);
		} break;
		default: {
			printf("Message with identifier %hhu has arrived.\n", packet->data[0]);
		} break;
		}

		m_xPeer->DeallocatePacket(packet);
		packet = m_xPeer->Receive();
	}
}

// This is the function responsible for initialising our game.
// When set to be a client, the function will ask the player for
// an address and a port, before attempting to connect to the address and port.
// When set to be a server, the function will start the server.
bool Client::Start()
{
	RakNet::SocketDescriptor socket = RakNet::SocketDescriptor();
	RakNet::ConnectionAttemptResult err;

	char address[16] = "localhost";
	char password[128] = "password";

	unsigned short port = 0;
	unsigned short num_conn = 0;

	printf("Enter server IP.\n");
	scanf("%15s", &address);

	while (port == 0) {
		printf("Enter port no.\n");
		scanf("%hu", &port);
	}

	printf("Enter handle.\n");
	scanf("%27s", m_csHandle);

	printf("Enter password.\n");
	scanf("%127s", &password);

	m_xPeer->Startup
	(
		1,
		&socket, 1
	);
	m_xLevel = level_create(c_uMaxClients);

	// (Server) Set the port up correctly:
	printf
	(
		"Connecting to server:\nAddress: %s\nPort: %hu\n",
		address,
		port
	);

	err = m_xPeer->Connect(address, port, password, strlen(password));
	printf("%s", _connectionAttemptResultCodes[err]);
	if (err != RakNet::CONNECTION_ATTEMPT_STARTED) {
		return false;
	}

	RakNet::RakNetGUID guid = m_xPeer->GetMyGUID();
	printf("GUID: %llu\n", guid.g);

	while (true) {
		RakNet::Packet *packet = m_xPeer->Receive();
		while(packet) {

			RakNet::MessageID message;

			RakNet::BitStream bsin(packet->data, packet->length, false);
			bsin.Read(message);

			if      (message == ID_CONNECTION_REQUEST_ACCEPTED) {
				printf("Connected successfully to server!\n");

				RakNet::BitStream bsout;
				puts("Sending credentials...");

				bsout.Write((unsigned char)ID_CLIENT_CREDENTIALS);
				bsout.Write(m_csHandle);

				m_xServerGUID = packet->guid;
				m_xPeer->Send(&bsout, IMMEDIATE_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}

			else if (message == ID_SERVER_CONNECTION_PASS) {
				RakNet::BitStream bsout;

				unsigned int timesconnected = 0;
				bool readinstuctions = true;
				char input[4];

				puts("Sucessfully connected to server!");
				bsin.Read(timesconnected);
				printf("Times connected: %u\n", timesconnected);

				if (timesconnected < 2) {
					printf("This is your first time player Scratch & Sniff!\nRead instructions? ");
					scanf("%s", &input);
					if (!strcmp(input, "no") || !strcmp(input, "No")) readinstuctions = false;
					if (readinstuctions) {

						printf("Welcome to Scratch & Sniff!\nControls:\n -w: Move Forward\n");
						printf(" -a: Move Left\n -s: Move Backwards\n -d: Move Right\n");
						printf("Hold the right mouse button to move the camera!\n");
						printf("Hit Enter to continue!");

						fflush(stdin);
						fgets(nullptr, 0, stdin);
					}
				}
				return true;
			}

			else if (message == ID_CONNECTION_ATTEMPT_FAILED) {
				printf("Connection attempt failed!\n");
				return false;
			}

			else if (message == ID_INVALID_PASSWORD) {
				printf("Invalid password!\n");
				return false;
			}

			else if (message == ID_CONNECTION_LOST) {
				printf("Lost connection during init loop!\n");
				return false;
			}

			else if (message == ID_SERVER_CONNECTION_USER_NAME_IN_USE) {
				printf("Error: That username is currently in use on this server! Please try another one.\n");
				return false;
			}

			else if (message == ID_SERVER_CONNECTION_PASS) {
				printf("Starting game...\n");
				return true;
			}

			else if (message == ID_DISCONNECTION_NOTIFICATION) {
				printf("Error: Server diconnected from us!\n");
				return false;
			}

			else if (message != ID_CONNECTION_REQUEST_ACCEPTED) {
				printf("Warning: Packet with ID %hhu arrived!\n", message);
			}

			m_xPeer->DeallocatePacket(packet);
			packet = m_xPeer->Receive();
		}
	}

	// Something terrible happened.
	printf("Error in Client::Start(): Left while(true) block!? Somehow?!\n");
	return false;
}

void Client::Update(float a_fDeltaTime)
{
	// Update our camera matrix using the keyboard/mouse:
	Utility::freeMovement(m_cameraMatrix, a_fDeltaTime, 10);

	// Poll our connection for packets:
	Network();

	unsigned int input = 0;
	static unsigned int oldinput = input;

	Input::Poll(input);
	if (input != oldinput) {
		oldinput = input;

//		unsigned int size = level_size(m_xLevel);
//		for (unsigned int i = 0; i < size; ++i)
//		{
//			Entity *ent = level_at(m_xLevel, i);
//
//			if (ent) {
//				ent->Input(input);
//			}
//		}

		RakNet::BitStream bsout;
		bsout.Write((RakNet::MessageID)ID_CLIENT_INPUT);
		bsout.Write(input);
		m_xPeer->Send
		(
			&bsout, HIGH_PRIORITY, UNRELIABLE_SEQUENCED,
			0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true
		);
	}

	unsigned int size = level_size(m_xLevel);
	for (unsigned int i = 0; i < size; ++i)
	{
		Entity *ent = level_at(m_xLevel, i);

		if (ent) {
			ent->Update(a_fDeltaTime);
		}
	}

	// Quit our application when escape is pressed:
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS) Quit();
}

void Client::Draw() 
{
	// Clear all gizmos from last frame:
	Gizmos::clear();

	// Clear the backbuffer:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// Get the view matrix from the world-space camera matrix:
	glm::mat4 viewMatrix = glm::inverse( m_cameraMatrix );
	
	// Add an identity matrix gizmo:
	Gizmos::addTransform(glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
	// Add a 20x20 grid on the XZ-plane:
	for (int i = 0; i < 21; ++i)
	{
		Gizmos::addLine(glm::vec3(-10 + i, 0, 10), glm::vec3(-10 + i, 0, -10),
			i == 10 ? glm::vec4(1, 1, 1, 1) : glm::vec4(0, 0, 0, 1));

		Gizmos::addLine(glm::vec3(10, 0, -10 + i), glm::vec3(-10, 0, -10 + i),
			i == 10 ? glm::vec4(1, 1, 1, 1) : glm::vec4(0, 0, 0, 1));
	}

	unsigned int size = level_size(m_xLevel);
	for (unsigned int i = 0; i < size; ++i)
	{
		Entity *ent = level_at(m_xLevel, i);

		if (ent) {
			ent->Draw();
		}
	}

	// Draw the gizmos from this frame:
	Gizmos::draw(viewMatrix, m_projectionMatrix);
}

bool Client::OnCreate()
{
	// initialise the Gizmos helper class:
	Gizmos::create();

	// Create a world-space matrix for a camera:
	m_cameraMatrix = glm::inverse(glm::lookAt(glm::vec3(10, 10, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)));

	// Create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio:
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, DEFAULT_SCREENWIDTH / (float)DEFAULT_SCREENHEIGHT, 0.1f, 1000.0f);

	// Set the clear colour and enable depth testing and backface culling:
	glClearColor(0.25f, 0.25f, 0.25f, 1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	return true;
}

void Client::Destroy()
{
	Gizmos::destroy();

	puts("Disconnecting...");

	RakNet::BitStream bsout;
	bsout.Write((unsigned char)ID_DISCONNECTION_NOTIFICATION);
	m_xPeer->Send(&bsout, IMMEDIATE_PRIORITY, RELIABLE, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	m_xPeer->Shutdown(10000);
}