#include <stdio.h>
#include <string.h>

#include "Client.hpp"

int main(void)//int argc, char *argv[])
{
	Client *client = nullptr;

	while (true) {
		client = new Client();

		if (client->Start()) {
			client->Run("Gamename", 800, 800, false);
		}
		delete(client);
	}
	
	client = nullptr;
	return EXIT_SUCCESS;
}