#ifndef __Application_H_
#define __Application_H_

#include "Common.h"
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "Application.h"
#include <glm/glm.hpp>
#include <map>

#include "Entity.hpp"
#include "Player.hpp"
#include "Level.h"

typedef unsigned int PlayerId;

// Derived application class that wraps up all globals neatly
class Client : public Application
{
public:
	Client();
	virtual ~Client();

	bool Start();
protected:
	virtual bool OnCreate();
	virtual void Update(float a_deltaTime);
	virtual void Draw();
	virtual void Destroy();

private:
	char m_csHandle[c_uNameLength];
	RakNet::RakPeerInterface *m_xPeer;
	RakNet::RakNetGUID m_xServerGUID;

	void DeSerializeEntities(RakNet::BitStream *a_xBitstream);
	void Network();
	void PrintStatus(void);

	glm::mat4 m_cameraMatrix;
	glm::mat4 m_projectionMatrix;

	LEVEL *m_xLevel = nullptr;
};

#endif // __Application_H_