#ifndef __Application_H_
#define __Application_H_

#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "Application.h"
#include <glm/glm.hpp>

// Server class for managing our scen
class Server
{
public:
	Server();
	~Server();

private:

	RakNet::RakPeerInterface *peer;
	void Create(unsigned short port, unsigned short maxClients);
};

// Derived application class that wraps up all globals neatly
class Client : public Application
{
public:

	Client();
	virtual ~Client();

protected:

	virtual bool OnCreate();
	virtual void Update(float a_deltaTime);
	virtual void Draw();
	virtual void Destroy();

	
	glm::mat4	m_cameraMatrix;
	glm::mat4	m_projectionMatrix;
};

#endif // __Application_H_