#ifndef PLAYER_INCLUDED
#define PLAYER_INCLUDED

#include <glm/ext.hpp>
#include "BitStream.h"
#include "Gizmos.h"

#include "Component.hpp"
#include "Input.hpp"
#include "Common.h"

// - Player.hpp -
// This component provides simple player functionality in do far
// as being able to move, using the inputs of a given ???:
class Player : public Component
{
	unsigned char m_hhuColor;

	float m_fMovespeed;

	// Player's have buffered input (allows for easier (de)serialization,
	// and results in automatic extrapolation:
	unsigned int m_uInput;

	RakNet::RakNetGUID m_xGUID;

public:

	unsigned int m_uScore;

	Player(const Entity * const a_xEntity, RakNet::RakNetGUID a_xGUID);

	virtual void Input(const unsigned int a_uInput);

	virtual void Update(const float a_fDeltaTime);

	virtual void OnPhysics()
	{

	}

	virtual void Draw();

	virtual void Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream)
	{
		if (a_bWrite) {
			a_xBitstream->Write(m_v3Pos.x);
			a_xBitstream->Write(m_v3Pos.y);
			a_xBitstream->Write(m_v3Pos.z);

			a_xBitstream->Write(m_uInput);
			a_xBitstream->Write(m_hhuColor);
			a_xBitstream->Write(m_xGUID);
			a_xBitstream->Write(m_uScore);
		}
		else {
			a_xBitstream->Read(m_v3Pos.x);
			a_xBitstream->Read(m_v3Pos.y);
			a_xBitstream->Read(m_v3Pos.z);

			a_xBitstream->Read(m_uInput);
			a_xBitstream->Read(m_hhuColor);
			a_xBitstream->Read(m_xGUID);
			a_xBitstream->Read(m_uScore);
		}
	}

	virtual void DecodePacket(RakNet::Packet *a_xPacket);

	virtual void Network(RakNet::RakPeerInterface *a_xPeer);

	void SetInput(const unsigned int a_uInput);

	void SetColor(const unsigned char a_hhuColor);

	glm::vec3 m_v3Pos;

	unsigned int Score(void) const;

	RakNet::RakNetGUID GUID() const;
};

#endif
