#ifndef ENTITY_INCLUDED
#define ENTITY_INCLUDED

#include "BitStream.h"
#include "Component.hpp"
#include "Common.h"

/*	- Entity -
	 This is the entity class. It's represents the named and identified
	aggregation of components (Translation: It's got a name, a unique
	identifier, and a component array).

	 The majority of functions that make up this class are overwhelmingly 
	iterator functions that go through each component in the class, and
	call some similarly named function in that component. The ones that
	aren't are probably just accessors for private variables...
*/

class Entity
{
private:
	char *m_sName;
	unsigned int m_uID;

	Component **m_vComponents = nullptr;
	unsigned int m_uCompCount = 0;
	unsigned int m_uCompLength = 0;

public:
	Entity(const char *a_sName, unsigned int m_uID);
	~Entity();

	void AddComponent(Component * a_xComponent)
	{
		if (!a_xComponent) return;

		if (m_uCompCount == m_uCompLength) {
			m_uCompLength *= 2;
			m_vComponents = (Component**)realloc(m_vComponents, sizeof(Component*) * m_uCompLength);
		}

		m_vComponents[m_uCompCount] = a_xComponent;
		++m_uCompCount;
	}
	void* GetComponent(ComponentID a_xType)
	{
		for (unsigned int i = 0; i < m_uCompCount; ++i)
		{
			if (m_vComponents[i] && m_vComponents[i]->m_eType == a_xType)
			{
				return m_vComponents[i];
			}
		}

		return nullptr;
	}

	void Input(const unsigned int a_uInput);
	void Update(const float a_fDeltaTime);
	void Network(RakNet::RakPeerInterface *a_xPeer);
	void Packet(RakNet::Packet *a_xPacket);
	void Draw();
	void Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream);

	const char * const Name() const;
	unsigned int ID() const;
};

#endif