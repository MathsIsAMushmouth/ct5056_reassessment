#ifndef PICKUP_INCLUDED
#define PICKUP_INCLUDED

#include "BitStream.h"
#include "Component.hpp"
#include "Common.h"

class Pickup : public Component
{

public:

	Pickup(const Entity * const a_xEntity, glm::vec3 a_v3Pos);

//	virtual void Update(const float a_fDeltaTime);

	virtual void OnPhysics()
	{

	}

	virtual void Draw();

	virtual void Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream)
	{
		if (a_bWrite) {
			a_xBitstream->Write(m_v3Pos.x);
			a_xBitstream->Write(m_v3Pos.y);
			a_xBitstream->Write(m_v3Pos.z);
		}
		else {
			a_xBitstream->Read(m_v3Pos.x);
			a_xBitstream->Read(m_v3Pos.y);
			a_xBitstream->Read(m_v3Pos.z);
		}
	}

	glm::vec3 m_v3Pos;
};

#endif