#ifndef RENDERER_INCLUDED
#define RENDERER_INCLUDED

#include "Bitstream.h"
#include "Component.hpp"
#include "Gizmos.h"

class Renderer : public Component
{
private:
	glm::vec3 m_vPos;
	glm::vec3 m_vBounds;

	// Used to update the scale of our object:
	//glm::vec3 oldbounds;

	bool m_bMove;
public:
	Renderer(const Entity * const a_xEntity, glm::vec3 a_vMin, glm::vec3 a_vMax);

	virtual void Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream);

	virtual void Update(const float a_fDeltaTime);
	virtual void Draw();
};

#endif