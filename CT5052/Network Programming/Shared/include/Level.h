#include "Entity.hpp"

typedef struct level LEVEL;

LEVEL* 
	level_create
	(const unsigned int a_uInitLength);

int
	level_add
	(LEVEL * const a_xPtr, const Entity * const ent);

void
	level_remove
	(LEVEL * const a_xPtr, const unsigned int a_uPos);

void
	level_removebyid
	(LEVEL * const a_xPtr, const unsigned int m_uID);

Entity*
	level_at
	(const LEVEL * const a_xPtr, const int a_uPos);

int
	level_findbyname
	(const LEVEL * const a_xPtr, const char * const a_csName);

int
	level_findbyid
	(const LEVEL * const a_xPtr, const unsigned int a_uID);

void
	level_destroy
	(LEVEL * const a_xPtr);

unsigned int
	level_size
	(const LEVEL * const a_xPtr);

void
	level_resize
	(LEVEL * const a_xPtr, const unsigned int a_xNewSize);