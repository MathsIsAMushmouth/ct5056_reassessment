#ifndef INPUT_INCLUDED
#define INPUT_INCLUDED

#define KEY_LEFT 0
#define KEY_RIGHT 1
#define KEY_UP 2
#define KEY_DOWN 3
#define MOUSE_0 4
#define MOUSE_1 5

// - Input.hpp -
// A collection of helper functions to make client input handling easier:
namespace Input
{
	// This function will get the state of all available inputs and store them
	// in the argmuent:
	void Poll(unsigned int &a_iInput);

	// Poll the state of a key in an input:
	bool Down(const unsigned char a_hhuID, const unsigned int a_uInput);
}

#endif