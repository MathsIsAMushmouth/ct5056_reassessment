#ifndef COMPONENT_INCLUDED
#define COMPONENT_INCLUDED

#include "BitStream.h"

#include "RakNetTypes.h"
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"

#include "Common.h"

// We forward declare the Entity class to prevent
// circular dependecy (at the expence of not having the entity
// defenition):
class Entity;
// Seriously, though, can that entity pointer be used for ANYTHING?

/*	- Component -
     This is the component class, and it can be thought of as the smallest
	divisible unit of behviour and code in the engine. The most important part
	of the component class are it's virtual functions.
	
	 Each of these virtual functions are used by the engine at various points 
	during runtime to give the component a chance to do something. This "some-
	thing" is where you come in(hopefully the game engine programmer).

	 In addition to providing virtual functions to override, this class has a
	type ID, for identification by other classes in the system(namely, Entity),
	and an entity pointer, which I haven't used yet, but I like to assume can
	in-fact be used for SOMETHING...
*/
class Component
{
public:
	Component(const ComponentID a_eID, const Entity * const a_xEntity) 
		: m_eType(a_eID), m_xEntity(a_xEntity)
	{}

	virtual ~Component() {};

	const ComponentID m_eType;
	const Entity * const m_xEntity;

	virtual void Input(const unsigned int a_uInput) {}
	virtual void Update(const float a_fDeltaTime) {}
	virtual void Draw(void) {}
	virtual void OnPhysics(void) {}
	virtual void Network(RakNet::RakPeerInterface *a_xPeer) {}
	virtual void DecodePacket(RakNet::Packet *a_xPacket) {}

	// I'm not sure why THIS one is declared pure virtual...
	virtual void Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream) = 0;
};

#endif