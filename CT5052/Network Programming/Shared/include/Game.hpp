#ifndef GAME_INCLUDED
#define GAME_INCLUDED

#include "Component.hpp"
#include "Common.h"
#include "Player.hpp"
#include "Level.h"
#include "Pickup.hpp"

class Game : public Component
{
public:
	Game(const Entity * const a_xEntity, LEVEL * const a_xLevel) : m_xLevel(a_xLevel),
		Component(GAME, a_xEntity)
	{}

	virtual void Update(const float a_fDeltaTime);

	virtual void Draw();

//	virtual void Network(RakNet::RakPeerInterface *a_xPeer);

	virtual void DecodePacket(RakNet::Packet *a_xPacket);

	virtual void Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitStream);

private:
	GameState m_eState = LOBBY;
	float m_fTimer = 0.0f;

	LEVEL * const m_xLevel = nullptr;

	// Uses Unique Identifiers:
	int m_aPlayerIDs[c_uMaxClients] = { 0 };
	unsigned int m_uNumPlayers = 0;

	int m_dPickupID = -1;

	int CreatePlayer(const char *a_csName, RakNet::RakNetGUID a_xGUID);
	int CreatePickup();
	bool IsWithin(Player *a_xPlayer, Pickup *a_xPickup);
};

#endif
