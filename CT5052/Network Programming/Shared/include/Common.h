#include "MessageIdentifiers.h"
#include "glm\ext.hpp"

#ifndef COMMON_INCLUDED
#define COMMON_INCLUDED

const unsigned int c_uMaxClients = 6;
const unsigned int c_uNameLength = 128;
const float        c_fStartCDown = 5.0f;
const float        c_fRoundTimer = 30.0f;
const float        c_fEndScrnTim = 10.0f;
const float        c_fPcktsPrSec = 15.0f;

const glm::vec3 c_v3PlrSpawnPos[6] = {
	glm::vec3(+10, 0, 0),
	glm::vec3(-10, 0, 0),
	glm::vec3(0, 0, +10),
	glm::vec3(0, 0, -10),
	glm::vec3(-7, 0, -7),
	glm::vec3(+7, 0, +7),
};

const glm::vec4 c_v4Colors[6] =
{
	glm::vec4(1, 0, 0, 1),
	glm::vec4(0, 1, 0, 1),
	glm::vec4(0, 0, 1, 1),
	glm::vec4(1, 1, 0, 1),
	glm::vec4(1, 0, 1, 1),
	glm::vec4(0, 1, 1, 1)
};

const glm::vec4 c_v4PickupColor = glm::vec4(1.0f, 0.8f, 0.1f, 1.0f);

/* Extention to the system defined types in RakNet (as found in MessageIdentifiers.h.)
   Used during packet transfer to identify unique packet types.
   (I'm pretty sure all of these are used... somewhere...)
*/
enum PacketType
{
	ID_SERVER_ENTITIES = ID_USER_PACKET_ENUM + 1,
	ID_CLIENT_INPUT,
	ID_SERVER_INPUT_BROADCAST,

	ID_SERVER_REQUEST_CLIENT_CREDENTIALS,
	ID_CLIENT_CREDENTIALS,

	ID_SERVER_CONNECTION_PASS,
	ID_SERVER_CONNECTION_USER_NAME_IN_USE,

	ID_CHANGE_STATE,
};

/* State enum user to identify the state of a connected system. */
enum GameState
{
	// Client: Tell connected user how many more clients needed before game starts.
	LOBBY = 0,
	// Server: Countdown to game starts; reposition players' on state-change;
	//         stop processing user inputs.
	// Client: Stop reciving/sending inputs to server; Tell connected user how 
	//         many seconds 'till the game starts.
	STARTING,
	// Server: Keep track of collectible item in field; add score to players on
	//         collection
	// Client: Display scores of currently connected users; display play-to
	//         objective.
	INGAME,
	// Server: Countdown to lobby-return.
	// Client: Display name of victorious player.
	ENDSCREEN
};

// These enum's make it easier to identify a component by type
// (Read's butter than an unsigned int).
enum ComponentID
{
	TRANSFORM,
	RENDERER,
	PLAYER,
	GAME,
	PICKUP
};

unsigned int
	GetNewID
	(void);

#endif