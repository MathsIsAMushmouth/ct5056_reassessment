#include "Renderer.hpp"

// MAP_SPEC for the total packet:
// byte 0: locks
//
// Each entry of 4 bytes after this will/could be
// a float value corresponding to one of the unlocked
// variables we'll be updating:

// We'll be using lock throughout this code to tell
// The program where to expect values:
// MAP_SPEC:
// 0: - Nothing - 
// 1: m_v3Pos.x get's updated:
// 2: m_v3Pos.y get's updated:
// 3: m_v3Pos.z get's updated:
// 4: m_vBounds.x get's updated:
// 5: m_vBounds.y get's updated:
// 6: m_vBounds.z get's updated:
// 7: - Nothing -

void Renderer::Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream)
{
	// Currently, we're only serializing the position:
	unsigned char locks = 0xF;

	if (a_bWrite) {

		// Write our lock variable. Currently, all the positions are updated all the time:
		a_xBitstream->Write(locks);
		a_xBitstream->Write(m_bMove);

		a_xBitstream->Write(m_vPos.x);
		a_xBitstream->Write(m_vPos.y);
		a_xBitstream->Write(m_vPos.z);
	}
	else {
		a_xBitstream->Read(locks);
		a_xBitstream->Read(m_bMove);

		// m_v3Pos updating:
		{
			if (locks & 1 << 1) {
				a_xBitstream->Read(m_vPos.x);
			}
			if (locks & 1 << 2) {
				a_xBitstream->Read(m_vPos.y);
			}
			if (locks & 1 << 3) {
				a_xBitstream->Read(m_vPos.z);
			}
		}
		// m_vBounds updating:
		{
			if (locks & 1 << 4) {
				a_xBitstream->Read(m_vBounds.x);
			}
			if (locks & 1 << 5) {
				a_xBitstream->Read(m_vBounds.y);
			}
			if (locks & 1 << 6) {
				a_xBitstream->Read(m_vBounds.z);
			}
		}
	}
}

Renderer::Renderer(const Entity * const a_xEntity, glm::vec3 a_vMin, glm::vec3 a_vMax)
	: Component(RENDERER, a_xEntity)
{
	m_vPos = a_vMin;
	m_vBounds = a_vMax;
}

void Renderer::Update(const float a_fDeltaTime)
{
	if (m_bMove)
	{
		m_vPos.x += 1 * a_fDeltaTime;
		if (m_vPos.x > 10) m_bMove = false;
	}
	else
	{
		m_vPos.x -= 1 * a_fDeltaTime;
		if (m_vPos.x < 0) m_bMove = true;
	}
}

void Renderer::Draw()
{
	Gizmos::addBox(m_vPos, m_vBounds, true);
}
