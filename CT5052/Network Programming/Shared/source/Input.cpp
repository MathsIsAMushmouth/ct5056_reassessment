#include "Input.hpp"

#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"

void Input::Poll(unsigned int &a_iInput)
{
	GLFWwindow *window = glfwGetCurrentContext();

	if (window)
	{
		// Assume no key's are pressed:
		a_iInput = 0;

		// Basic Movement:
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			a_iInput |= 1 << KEY_UP;
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			a_iInput |= 1 << KEY_LEFT;
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			a_iInput |= 1 << KEY_DOWN;
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			a_iInput |= 1 << KEY_RIGHT;
		}

		// Mouse Buttons:
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {
			a_iInput |= 1 << MOUSE_0;
		}
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS) {
			a_iInput |= 1 << MOUSE_1;
		}
	}
}

bool Input::Down(const unsigned char a_hhuID, const unsigned int a_iInput)
{
	return a_iInput & (1 << a_hhuID);
}