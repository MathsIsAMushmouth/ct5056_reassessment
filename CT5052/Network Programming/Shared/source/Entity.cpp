#include "Entity.hpp"
#include "Common.h"

Entity::Entity(const char *a_sName, unsigned int a_uID) : m_uID(GetNewID())
{
	m_sName = (char*)malloc(strlen(a_sName));
	m_uID = a_uID;

	if (m_sName) {
		strcpy(m_sName, a_sName);
	}

	m_uCompCount = 0;
	m_uCompLength = 4;
	m_vComponents = (Component**)malloc(sizeof(Component*) * m_uCompLength);
}

Entity::~Entity()
{
	for (unsigned int i = 0; i < m_uCompCount; ++i) {
		if (m_vComponents[i]) free(m_vComponents[i]);
	}
}

void Entity::Input(const unsigned int a_uInput)
{
	for (unsigned int i = 0; i < m_uCompCount; ++i)
	{
		if (m_vComponents[i]) {
			m_vComponents[i]->Input(a_uInput);
		}
	}
}

void Entity::Update(const float a_fDeltaTime)
{
	for (unsigned int i = 0; i < m_uCompCount; ++i)
	{
		if (m_vComponents[i]) {
			m_vComponents[i]->Update(a_fDeltaTime);
		}
	}
}

void Entity::Network(RakNet::RakPeerInterface *a_xPeer)
{
	if (!a_xPeer) return;

	for (unsigned int i = 0; i < m_uCompCount; ++i)
	{
		if (m_vComponents[i]) {
			m_vComponents[i]->Network(a_xPeer);
		}
	}
}

void Entity::Packet(RakNet::Packet *a_xPacket)
{
	if (!a_xPacket) return;

	for (unsigned int i = 0; i < m_uCompCount; ++i)
	{
		if (m_vComponents[i]) {
			m_vComponents[i]->DecodePacket(a_xPacket);
		}
	}
}

void Entity::Draw()
{
	for (unsigned int i = 0; i < m_uCompCount; ++i)
	{
		if (m_vComponents[i]) {
			m_vComponents[i]->Draw();
		}
	}
}

void Entity::Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitstream)
{
	// Write the name to the stream:
//	if (a_bWrite) {		
		a_xBitstream->Write(m_uID);
		a_xBitstream->Write(m_sName);
		a_xBitstream->Write(m_uCompCount);
//		a_xBitstream->Write(m_uCompLength);
//	}
//	else {
//		a_xBitstream->Read(m_sName);
//		a_xBitstream->Read(m_uCompCount);
//		a_xBitstream->Read(m_uCompLength);
//	}
//
//	if (!a_bWrite) {
//		m_vComponents = (Component**)malloc(sizeof(Component*) * m_uCompLength);
//	}
//

	for (unsigned int i = 0; i < m_uCompCount; ++i)
	{
		if (m_vComponents[i]) {
//			if (a_bWrite) {
				a_xBitstream->Write(m_vComponents[i]->m_eType);
//			}
//			else {
//				a_xBitstream->Read(m_vComponents[i]->m_eType);
//			}
//		
			m_vComponents[i]->Serialize(a_bWrite, a_xBitstream);
		}
	}
}

const char * const Entity::Name() const
{
	return m_sName;
}

unsigned int Entity::ID() const
{
	return m_uID;
}
