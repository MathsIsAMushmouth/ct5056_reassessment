#include "Level.h"

struct level
{
	Entity *m_aEntities = nullptr;
	// The number of entities assigned to this object:
	unsigned int m_uLength = 0;
	// The number of entities that can be used/indexed by this object:
	unsigned int m_uCount = 0;
};

LEVEL* level_create(const unsigned int a_uInitLength)
{
	LEVEL *out = (LEVEL*)malloc(sizeof(LEVEL));

	if (out) {
		out->m_uCount    = 0;
		out->m_uLength   = a_uInitLength;
		out->m_aEntities = (Entity*)calloc(a_uInitLength, sizeof(Entity));
	}

	return out;
}

// This function makes a perfect copy of m_xEntity in the given level, then
// returns the position in the level of the new entity.
//
//  An important thing to note about this function is that it does not 
// free the memory used by the given entity; when you use this function, 
// you'll want to make sure you dispose of the old entity properly.
int
level_add(LEVEL * const a_xPtr, const Entity * const a_xEntity)
{
	if (!a_xPtr) {
		return -1;
	}
	if (!a_xEntity) {
		return -2;
	}
	
	if (a_xPtr->m_uCount == a_xPtr->m_uLength) {
		a_xPtr->m_uLength *= 2;
		a_xPtr->m_aEntities = (Entity*)realloc(a_xPtr->m_aEntities, sizeof(Entity) * a_xPtr->m_uLength);
	}

	memcpy(a_xPtr->m_aEntities + a_xPtr->m_uCount, a_xEntity, sizeof(Entity));
	++a_xPtr->m_uCount;

	// Return the offset of the new entity:
	return a_xPtr->m_uCount - 1;
}

int
level_findbyname(const LEVEL * const a_xPtr, const char * const a_csName)
{
	if (!a_xPtr) {
		return -1;
	}
	else {
		unsigned int size = level_size(a_xPtr);
		for (unsigned int i = 0; i < size; ++i) {
			if (!strcmp(a_xPtr->m_aEntities[i].Name(), a_csName)) {
				return i;
			}
		}
	}

	return -2;
}

int
level_findbyid(const LEVEL * const a_xPtr, const unsigned int a_uID)
{
	if (!a_xPtr) {
		return -1;
	}
	else {
		unsigned int size = level_size(a_xPtr);
		for (unsigned int i = 0; i < size; ++i) {
			if (a_xPtr->m_aEntities[i].ID() == a_uID) {
				return i;
			}
		}
	}

	return -2;
}

void
level_remove(LEVEL * const a_xPtr, const unsigned int a_uPos)
{
	if (!a_xPtr) {
		return;
	}
	else if (a_uPos < a_xPtr->m_uCount) {

		// I like how in this ocean of more-or-less perfect C code, the single 
		// most ugly line of code is the only line of C++:
		a_xPtr->m_aEntities[a_uPos].~Entity();
		
		memmove
		(
			a_xPtr->m_aEntities + a_uPos,
			a_xPtr->m_aEntities + a_uPos + 1,
			sizeof(Entity) * a_xPtr->m_uLength - (a_uPos + 1)
		);

		--a_xPtr->m_uCount;
	}
}

void
level_removebyid(LEVEL * const a_xPtr, const unsigned int m_uID)
{
	if (!a_xPtr) {
		return;
	}
	else for (unsigned int i = 0; i < a_xPtr->m_uCount; ++i) {

		if (a_xPtr->m_aEntities[i].ID() == m_uID) {
			
			a_xPtr->m_aEntities[i].~Entity();

			memmove
			(
				a_xPtr->m_aEntities + i, 
				a_xPtr->m_aEntities + i + 1, 
				sizeof(Entity) * a_xPtr->m_uLength - (i + 1)
			);

			--a_xPtr->m_uCount;
		}
	}
}

Entity*
level_at(const LEVEL * const a_xPtr, const int a_uPos)
{
	if (!a_xPtr || a_uPos < 0) {
		return nullptr;
	}
	else if (a_uPos < a_xPtr->m_uCount) {
		return a_xPtr->m_aEntities + a_uPos;
	}
	else {
		return nullptr;
	}
}

void 
level_destroy(LEVEL *ptr)
{
	if (ptr) {
		free(ptr->m_aEntities);
		free(ptr);
	}
}

unsigned int
level_size(const LEVEL * const a_xPtr)
{
	if (a_xPtr) return a_xPtr->m_uCount;
	else return 0;
}

void
level_resize(LEVEL * const a_xPtr, const unsigned int a_xNewSize)
{
	if (!a_xPtr) return;

	a_xPtr->m_aEntities = (Entity*)realloc(a_xPtr->m_aEntities, sizeof(Entity) * a_xNewSize);
}