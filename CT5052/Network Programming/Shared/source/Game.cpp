#include "Game.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

void 
Game::Update(const float a_fDeltaTime)
{
	if (m_fTimer > 0) {
		m_fTimer -= a_fDeltaTime;
	}
	else if (m_fTimer < 0) {
		m_fTimer = 0.0f;
	}

	// Update our game state:
	if (m_uNumPlayers == c_uMaxClients && m_eState == LOBBY) {
		m_eState = STARTING;
		m_fTimer = c_fStartCDown;

		// Reset player positions:
		for (unsigned int i = 0; i < m_uNumPlayers; ++i) {
			Entity *ent = level_at(m_xLevel, level_findbyid(m_xLevel, m_aPlayerIDs[i]));
			if (ent) {
				Player *plr = (Player*)ent->GetComponent(PLAYER);
				if (plr) {
					plr->m_v3Pos = c_v3PlrSpawnPos[i % 6];
				}
			}
		}

		CreatePickup();
	}
	else if (m_eState == STARTING && m_fTimer == 0) {
		m_eState = INGAME;
		m_fTimer = c_fRoundTimer;
	}
	else if (m_eState == INGAME && m_fTimer == 0) {
		m_eState = ENDSCREEN;
		m_fTimer = c_fEndScrnTim;
	}
	else if (m_eState == ENDSCREEN && m_fTimer == 0) {
		m_eState = LOBBY;
		m_fTimer = 0.0f;
	}

	// Try pickup-player collisions:
	if (m_eState == INGAME && m_dPickupID > -1) {

		unsigned int pickupPos = level_findbyid(m_xLevel, m_dPickupID);

		Entity *ent = level_at(m_xLevel, pickupPos);
		if (!ent) return;

		Pickup *pickup = (Pickup*)ent->GetComponent(PICKUP);
		if (!pickup) return;

		for (unsigned int i = 0; i < m_uNumPlayers; ++i) {

			unsigned int playerID = m_aPlayerIDs[i];
			unsigned int playerPos = level_findbyid(m_xLevel, playerID);

			ent = level_at(m_xLevel, playerPos);
			if (!ent) break;

			Player *player = (Player*)ent->GetComponent(PLAYER);
			if (!player) break;

			if (IsWithin(player, pickup)) {
				printf("Player collided with pickup!\n");
				++player->m_uScore;
				CreatePickup();
			}
		}
	}
}

void 
Game::Draw()
{
	ImGui::Begin("Menu");
	switch (m_eState) {
	case LOBBY:
		if (m_uNumPlayers > 1) {
			ImGui::Text("Waiting for %d more players...", c_uMaxClients - m_uNumPlayers);
		}
		else {
			ImGui::Text("Waiting for %d more player...", c_uMaxClients - m_uNumPlayers);
		}
		break;
	case STARTING:
		ImGui::Text("Starting game in...");
		ImGui::SameLine();
		ImGui::Text("%g", m_fTimer);
		break;
	case INGAME:
		ImGui::Text("Time: %.0f", m_fTimer);
		break;
	case ENDSCREEN:
		ImGui::Text("Game Over!");
		ImGui::Text("---------------------------");
		for (unsigned int i = 0; i < m_uNumPlayers; ++i) {
			Entity *ent = level_at(m_xLevel, level_findbyid(m_xLevel, m_aPlayerIDs[i]));
			if (ent) {
				Player *plr = (Player*)ent->GetComponent(PLAYER);
				if (plr) {
					ImGui::Text("%s : %u", ent->Name(), plr->Score());
				}
			}
		}
		break;
	default:
		ImGui::Text("Current state: %u", m_eState);
	}
	ImGui::End();
}

void 
Game::DecodePacket(RakNet::Packet *a_xPacket)
{
	if (!a_xPacket) return;
	RakNet::BitStream bsin(a_xPacket->data, a_xPacket->length, false);

	RakNet::MessageID id;
	bsin.Read(id);

	switch (id) {
		case ID_CLIENT_CREDENTIALS: {
			RakNet::RakString name;
			bsin.Read(name);

			// Create a player with the signature of this client only.
			// We'll leave it for the server to decide whether this
			// client should be disconnected or not:
			CreatePlayer(name.C_String(), a_xPacket->guid);
		} break;
		case ID_DISCONNECTION_NOTIFICATION: {
			printf("Disconnecting user...\n");
			printf("Packet ID: %llu\n", a_xPacket->guid.g);
			printf("Num players: %u\n", m_uNumPlayers);
			for (unsigned int i = 0; i < m_uNumPlayers; ++i) {
				
				unsigned int playerID = m_aPlayerIDs[i];
				unsigned int playerPos = level_findbyid(m_xLevel, playerID);

				printf("Player ID: %u\nPlayer Pos: %u\n", playerID, playerPos);

				Entity *ent = level_at(m_xLevel, playerPos);
				if (ent) {
					Player *player = (Player*)ent->GetComponent(PLAYER);
					if (player) {
						printf("Client ID: %llu\n", player->GUID().g);

						if (player->GUID() == a_xPacket->guid) {
							level_removebyid(m_xLevel, m_aPlayerIDs[i]);
							--m_uNumPlayers;
							break;
						}
					}
				}
			}
		} break;
	}
}

void 
Game::Serialize(const bool a_bWrite, RakNet::BitStream *a_xBitStream)
{
	if (a_bWrite) {
		a_xBitStream->Write(m_eState);
		a_xBitStream->Write(m_fTimer);
		a_xBitStream->Write(m_uNumPlayers);
		for (unsigned int i = 0; i < m_uNumPlayers; ++i) {
			a_xBitStream->Write(m_aPlayerIDs[i]);
		}
	}
	else {
		a_xBitStream->Read(m_eState);
		a_xBitStream->Read(m_fTimer);
		a_xBitStream->Read(m_uNumPlayers);
		for (unsigned int i = 0; i < m_uNumPlayers; ++i) {
			a_xBitStream->Read(m_aPlayerIDs[i]);
		}
	}
}

int
Game::CreatePlayer(const char *a_csName, RakNet::RakNetGUID a_xGUID)
{
	if (m_uNumPlayers >= c_uMaxClients) return -4;
	if (!a_csName) return -3;

	printf("Name passed to CreatePlayer: %s\n", a_csName);
	printf("GUID passed to CreatePlayer: %llu\n", a_xGUID.g);
	
	Player *player = nullptr;
	int err = 0;

	// Create an entity:
	err = level_add(m_xLevel, &Entity(a_csName, GetNewID()));
	Entity *ent = level_at(m_xLevel, err);

	if (ent) {
		m_aPlayerIDs[m_uNumPlayers] = ent->ID();
		++m_uNumPlayers;

		Player *player = new Player(ent, a_xGUID);
		
		player->SetColor(m_uNumPlayers);
		player->m_v3Pos = c_v3PlrSpawnPos[m_uNumPlayers % 6];

		ent->AddComponent(player);
	}

	if (err < 0) {
		printf("Error in level_add: Code %u!", err);
	}

	// Return the unique ID of the player:
	return ent->ID();
}

int
Game::CreatePickup()
{
	if (m_dPickupID >= 0) level_removebyid(m_xLevel, m_dPickupID);

	int err = level_add(m_xLevel, &Entity("Pickup", GetNewID()));
	if (err > -1) {

		Entity *ent = level_at(m_xLevel, err);
		if (ent) {
			Pickup *pickup = new Pickup(ent, glm::vec3(glm::linearRand(-10, 10), 0, glm::linearRand(-10, 10)));

			m_dPickupID = ent->ID();

			ent->AddComponent(pickup);
			return m_dPickupID;
		}
	}

	return 0;
}

bool Game::IsWithin(Player * a_xPlayer, Pickup * a_xPickup)
{
	if (!a_xPlayer || !a_xPickup) return false;

	if (a_xPlayer->m_v3Pos.x + 1 < a_xPickup->m_v3Pos.x - 0.5f) return false;
	if (a_xPlayer->m_v3Pos.x - 1 > a_xPickup->m_v3Pos.x + 0.5f) return false;
	if (a_xPlayer->m_v3Pos.y + 1 < a_xPickup->m_v3Pos.y - 0.5f) return false;
	if (a_xPlayer->m_v3Pos.y - 1 > a_xPickup->m_v3Pos.y + 0.5f) return false;
	if (a_xPlayer->m_v3Pos.z + 1 < a_xPickup->m_v3Pos.z - 0.5f) return false;
	if (a_xPlayer->m_v3Pos.z - 1 > a_xPickup->m_v3Pos.z + 0.5f) return false;

	return true;
}
