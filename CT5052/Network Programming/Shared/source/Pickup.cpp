#include "Pickup.hpp"
#include "Gizmos.h"

Pickup::Pickup(const Entity * const a_xEntity, glm::vec3 a_v3Pos) : Component(PICKUP, a_xEntity)
{
	m_v3Pos = a_v3Pos;
}

void Pickup::Draw()
{
	Gizmos::addBox(m_v3Pos, glm::vec3(0.5f, 0.5f, 0.5f), true, c_v4PickupColor);
}
