#include "Common.h"

unsigned int
GetNewID(void)
{
	static unsigned int id = 0;

	unsigned int out = id;
	++id;

	return out;
}