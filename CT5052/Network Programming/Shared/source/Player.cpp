#include "Player.hpp"

Player::Player(const Entity * const a_xEntity, RakNet::RakNetGUID a_xGUID)
	: Component(PLAYER, a_xEntity)
{
	m_fMovespeed = 5;

	m_v3Pos = glm::vec3(0, 0, 0);

	m_uInput = 0;

	m_xGUID = a_xGUID;

	m_uScore = 0;
}

void Player::Input(unsigned int a_uInput)
{
	m_uInput = a_uInput;
}

void Player::Update(const float a_fDeltaTime)
{
	if (m_uInput == 0) return;

	if (!Input::Down(MOUSE_1, m_uInput))
	{
		if (Input::Down(KEY_RIGHT, m_uInput)) {
			m_v3Pos.x += m_fMovespeed * a_fDeltaTime;
		}
		if (Input::Down(KEY_LEFT, m_uInput)) {
			m_v3Pos.x -= m_fMovespeed * a_fDeltaTime;
		}
		if (Input::Down(KEY_DOWN, m_uInput)) {
			m_v3Pos.z += m_fMovespeed * a_fDeltaTime;
		}
		if (Input::Down(KEY_UP, m_uInput)) {
			m_v3Pos.z -= m_fMovespeed * a_fDeltaTime;
		}
	}
}

void Player::Draw()
{
	Gizmos::addBox(m_v3Pos, glm::vec3(1, 1, 1), true, c_v4Colors[m_hhuColor % 6]);
}

void Player::DecodePacket(RakNet::Packet *a_xPacket)
{
	if (!a_xPacket) return;
	RakNet::BitStream bsin(a_xPacket->data, a_xPacket->length, false);

	RakNet::MessageID id;
	bsin.Read(id);

	switch (id) {
	case ID_CLIENT_INPUT: {
		printf("Recieving input...\n");
		printf("Packet ID: %llu\nClient ID: %llu\n", a_xPacket->guid.g, m_xGUID.g);
		if (m_xGUID == a_xPacket->guid) {

			bsin.Read(m_uInput);
		}
	} break;
	}
}

void Player::Network(RakNet::RakPeerInterface *a_xPeer)
{

}

void Player::SetInput(const unsigned int a_uInput)
{
	m_uInput = a_uInput;
}

void Player::SetColor(const unsigned char a_hhuColor)
{
	m_hhuColor = a_hhuColor;
}

unsigned int Player::Score(void) const
{
	return m_uScore;
}

RakNet::RakNetGUID Player::GUID() const
{
	return m_xGUID;
}